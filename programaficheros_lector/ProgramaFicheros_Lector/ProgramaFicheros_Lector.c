#include <stdio.h>
#include <stdlib.h>

int main()
{
	FILE *fichero;
	char caracter;

	fichero = fopen("FicheroCreado.txt", "r");

	if (fichero == NULL)
	{
		printf("\nNo se ha podido abrir el fichero.\n");
		system("PAUSE");
	}
	else
	{
		printf("\nFichero abierto.\n");
		printf("\nEl contenido del fichero es:\n\n");

		while ((caracter = fgetc(fichero)) != EOF)
		{
			printf("%c", caracter);
		}

		printf("\n\n");
	}

	fclose(fichero);

	system("PAUSE");

	return 0;
}