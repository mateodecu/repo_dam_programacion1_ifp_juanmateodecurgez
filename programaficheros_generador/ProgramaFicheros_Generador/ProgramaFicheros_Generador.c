#include <stdio.h>
#include <stdlib.h>

#define TAMANIO_DATA 1000

int main()
{
	char data[TAMANIO_DATA];

	FILE *fichero;

	fichero = fopen("FicheroCreado.txt", "w");

	if (fichero == NULL)
	{
		printf("\nNo se ha podido crear el fichero.\n");
		system("PAUSE");
	}
	else
	{
		for (int i = 1; i <= 5; i++)
		{
			sprintf(data, "%d", i);
			fputs(data, fichero);
			fputs(data, fichero);
			fputs(data, fichero);
			fputc('\n', fichero);
		}
	}

	fclose(fichero);
	
	printf("\nEl fichero se ha creado exitosamente.\n");

	system("PAUSE");

	return 0;
}