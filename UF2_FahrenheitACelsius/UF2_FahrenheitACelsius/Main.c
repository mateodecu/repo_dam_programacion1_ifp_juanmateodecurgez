﻿#include <stdio.h>

double CelsiusAFahrenheit(double gradosC)
{
	return (gradosC*9/5)+32;
}

double FahrenheitACelsius(double gradosF)
{
	return (gradosF - 32)*5/9;
}

void main() {
	printf("Selecione, ingresando el numero de opcion, el tipo de conversion que desea realizar. Luego valide la seleccion pulsando Enter.\n1) Grados Celsius a Fahrenheit\n2) Grados Fahrenheit a Celsius\n");

	int opcion;
	scanf_s("%d", &opcion);

	while (opcion < 1 || opcion > 2)
	{
		printf("El numero de opcion ingresado es incorrecto. Por favor, vuelva a ingresar la opcion.\n");
		scanf_s("%d", &opcion);
	}

	switch (opcion)
	{
	case 1:
		printf("Conversion de grados Celsius a Fahrenheit\nIngrese grados Celsius: ");
		
		double gradosC;
		scanf_s("%lf", &gradosC);

		printf("Su equivalente en grados Fahrenheit es: %lf F\n", CelsiusAFahrenheit(gradosC));
		
		break;
	case 2:
		printf("Conversion de grados Fahrenheit a Celsius\nIngrese grados Fahrenheit: ");
		
		double gradosF;
		scanf_s("%lf", &gradosF);
		
		printf("Su equivalente en grados Celsius es: %lf C\n", FahrenheitACelsius(gradosF));
		
		break;
	}

	system("pause");
}