#include <stdio.h>

double Suma(double primerN, double segundoN)
{
	return primerN + segundoN;
}

double Resta(double primerN, double segundoN)
{
	return primerN - segundoN;
}

double Multiplicacion(double primerN, double segundoN)
{
	return primerN * segundoN;
}

double Division(double primerN, double segundoN)
{
	return primerN / segundoN;
}

void main() {

	printf("Operacion de numeros.\n");
	
	printf("Ingrese el primer numero y valide presionando Enter.\n");
	double primerN;
	scanf_s("%lf", &primerN);

	printf("Ingrese el segundo numero y valide presionando Enter.\n");
	double segundoN;
	scanf_s("%lf", &segundoN);
	
	printf("Ingrese el tipo de operacion que desea realizar y valide presionando Enter.\n1) Suma\n2) Resta\n3) Multiplicacion\n4) Division\n");
	int opcion;
	scanf_s("%d", &opcion);

	while (opcion < 1 || opcion > 4)
	{
		printf("El numero de opcion ingresado es incorrecto. Por favor, vuelva a ingresar la opcion.\n");
		scanf_s("%d", &opcion);
	}

	switch (opcion)
	{
	case 1:
		printf("El resultado de la Suma de los numeros ingresados es: %lf\n", Suma(primerN, segundoN));
		break;
	case 2:
		printf("El resultado de la Resta de los numeros ingresados es: %lf\n", Resta(primerN, segundoN));
		break;
	case 3:
		printf("El resultado de la Multiplicacion de los numeros ingresados es: %lf\n", Multiplicacion(primerN, segundoN));
		break;
	case 4:
		printf("El resultado de la Division de los numeros ingresados es: %lf\n", Division(primerN, segundoN));
		break;
	}

	system("pause");
}